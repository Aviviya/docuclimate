import { useState, useEffect } from "react";
import internet_cloud from './internet_cloud.svg';
import './App.css';
import WeatherDisplay from "./components/WeatherDisplay";


import React from 'react';
import logo from './logo.svg';



import { withAuthenticator, AmplifySignOut } from '@aws-amplify/ui-react'
import NoteForm from "./components/NoteForm";


//-----------------------------------------------------------------------------------//
function LandingPage() {


  //New state variable and function
  const [noteVisibility, setNoteVisibility] = useState(false);
  const [apiData, setApiData] = useState({});

  //Declairing a new state variable, and function to handle
  const [note, setNote] = useState("");

  //Creating a function to handle toggling the note visibility
  const toggleNoteVisibility = () => {
    if (noteVisibility === false) {
      setNoteVisibility(true);
    } else {
      setNoteVisibility(false);
    }

    console.log("Note Visible", noteVisibility);
  };

  //Creating an "onChange" function to handle the user typing text into the textarea
  const handleInput = (event) => {
    console.log("Value Changed", event.target.value); //handleInput accepts value of an event, and outputs the value
    setNote(event.target.value); //passing the value entered as a note
  };

  //Creating a "fat arrow" function to work better w/ React
  const saveNote = () => {
    console.log("Note Saved ", note);
  };

  //--------------------------------------------------------------------------------------//

  return (
    <div className="App">
      <header className="App-header">
        {/* Some temporary/dummy data under the main header/title */}
        <h1 id="topHeader">DocuClimate</h1>

        <WeatherDisplay apiData={apiData} setApiData={setApiData}></WeatherDisplay>

        <img src={internet_cloud} className="App-logo" alt="logo" />

        <NoteForm apiData={apiData}></NoteForm>
      </header>
    </div>
  );
}


export default withAuthenticator(LandingPage);
