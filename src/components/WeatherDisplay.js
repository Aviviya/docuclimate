import { useEffect } from "react";

function WeatherDisplay(props) {
    const { apiData, setApiData } = props;
    //API key and URL
    const apiKey = process.env.REACT_APP_API_KEY;
    const apiUrl = `https://api.openweathermap.org/data/2.5/weather?zip=${99354}&appid=${apiKey}`;

    //Side effect
    useEffect(() => {
        getWeather();
    }, []);

    //Function to convert kalvin to farenheit
    const kelvinToFarenheit = (k) => {
        return ((k - 273.15) * (9 / 5) + 32).toFixed(2);
    };

    const getWeather = () => {
        fetch(apiUrl)
            .then((res) => res.json())
            .then((data) => setApiData(data));
    };
    const saveNote = (temp, humidity, currentConditions) => { };
    return (
        <div>
            <div>
                Current Temperature:{" "}
                {apiData.main ? kelvinToFarenheit(apiData.main.temp) : ""} degress F
      </div>
            <div>
                Weather Conditions:{" "}
                {apiData.weather ? apiData.weather[0].description : ""}
            </div>
        </div>
    );
}

export default WeatherDisplay;